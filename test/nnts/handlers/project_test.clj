(ns nnts.handlers.project-test
  (:require [nnts.handlers.project :as sut]
            [nnts.models.project :as proj]
            [nnts.models.user :as user]
            [nnts.models.organisation :as org]
            [nnts.helpers.factory :as factory]
            [nnts.handlers.helper :as resp-helper]
            [nnts.helpers.config :as conf-helper]
            [nnts.db.core :as db-core]
            [nnts.config :as config]
            [clojure.test :refer :all]
            [clojure.walk :refer (stringify-keys)]
            [ring.adapter.jetty :as jetty]
            [clj-http.client :as client]
            [clj-time.format :as fmt]
            [failjure.core :as f]
            [nnts.models.helper :as helper]
            [nnts.db.helper :as db-helper]))


(def invalid-uuid (factory/get-invalid-uuid))


(defn- get-new-request-for-project
  "Return a mock request object for projects."
  []
  {:form-params (stringify-keys (dissoc (factory/get-new-project) :id :organisation_id))
   :authenticated-user-id (factory/get-new-uuid)
   :route-params {:organisation-slug "valid-org-slug"}
   })


(defn- get-new-request-for-project-member
  "Return a mock request object for project member addition."
  []
  {:form-params {"user-id" (str (factory/get-new-uuid))}
   :authenticated-user-id (factory/get-new-uuid)
   :route-params {:organisation-slug "valid-org-slug" :project-slug "valid-project-slug"}})


(defn- get-new-request-for-project-note
  "Return a new mock request object for project note."
  [note]
  {:form-params {"note_date" (:note_date note)
                 "title" (:title note)
                 "body" (:body note)}
   :authenticated-user-id (factory/get-new-uuid)
   :route-params {:organisation-slug "valid-org-slug" :project-slug "valid-project-slug"}})

(defn- get-new-request-for-view-notes
  [note]
  {:query-params {"note-date" (:note_date note)
                  "project-id" (:project_id note)}
   :authenticated-user-id (factory/get-new-uuid)})


(def project-atom (atom []))

(def org-id-atom (atom []))

(def project-member-atom (atom []))

(def project-note-atom (atom []))

(use-fixtures
  :each
  (fn [test-fn]
    (reset! org-id-atom [])
    (reset! project-atom [])
    (reset! project-member-atom [])
    (reset! project-note-atom [])
    (with-redefs [org/validate-organisation-by-slug (fn [org-slug logged-user-id]
                                                      (if (and (= "valid-org-slug" org-slug)
                                                               (not= invalid-uuid logged-user-id))
                                                        (do
                                                          (swap! org-id-atom conj (factory/get-new-uuid))
                                                          (first @org-id-atom))
                                                        (f/fail (helper/construct-model-error :invalid-org-slug))))

                  proj/create-project (fn [proj]
                                        (when (not (= :clojure.spec.alpha/invalid proj))
                                          (swap! project-atom conj proj)))

                  proj/validate-project-by-slug (fn [proj-slug valid-org-id]
                                                  (if (and (= "valid-project-slug" proj-slug)
                                                           (not= invalid-uuid valid-org-id))
                                                    valid-org-id
                                                    (f/fail (helper/construct-model-error :invalid-project-slug))))

                  proj/add-member-to-project (fn [member]
                                               (if-not (some #(= % member) @project-member-atom)
                                                 (swap! project-member-atom conj member)
                                                 (f/fail (helper/construct-model-error :project-member-already-exists))))

                  user/validate-user-id (fn [user-id]
                                          user-id)

                  proj/get-project-owner-or-member (fn [project-id user-id]
                                                     (when (and project-id user-id)
                                                       user-id))

                  proj/create-note (fn [note]
                                     (when-not (= :clojure.spec.alpha/invalid note)
                                       (swap! project-note-atom conj note)))

                  proj/edit-note (fn [id note]
                                   (when-not (= :clojure.spec.alpha/invalid note)
                                     (reset! project-note-atom [note])))]
      (test-fn))))


(deftest test-create-project
  (testing "Create project must succeed for valid data."
    (let [req (get-new-request-for-project)]
      (let [res (sut/post-create-project req)]
        (is (= 201 (:status res)))
        (is (= :project-create-success (get-in res [:body :status])))
        (is (= 1 (count @project-atom)))))))


(deftest test-create-project-invalid-name
  (testing "Create project must fail for invalid project name."
    (let [req (assoc-in (get-new-request-for-project) [:form-params "name"] :imie)]
      (let [res (sut/post-create-project req)]
        (is (= 500 (:status res)))
        (is (= :project-data-invalid-format
               (get-in res [:body :status])))
        (is (= 0 (count @project-atom)))))))


(deftest test-create-project-invalid-org
  (testing "Create project must fail for invalid organisation."
    (let [req (assoc-in (get-new-request-for-project) [:route-params :organisation-slug] "invalid-org-slug")]
      (let [res (sut/post-create-project req)]
        (is (= 500 (:status res)))
        (is (= :invalid-org-slug (get-in res [:body :status])))
        (is (= 0 (count @project-atom)))))))


(deftest test-add-project-member
  (testing "Adding a valid user to a valid project should succeed."
    (let [req (get-new-request-for-project-member)]
      (let [res (sut/post-create-member req)]
        (is (= 201 (:status res)))
        (is (= :project-member-add-success (get-in res [:body :status])))
        (is (= 1 (count @project-member-atom)))))))


(deftest test-add-project-duplicate-member
  (testing "Adding a valid user to a valid project more than once should fail."
    (let [req (get-new-request-for-project-member)]
      (let [res (sut/post-create-member req)]
        (is (= 201 (:status res)))
        (is (= :project-member-add-success (get-in res [:body :status])))
        (is (= (count @project-member-atom) 1)))
      (let [dup-res (sut/post-create-member req)]
        (is (= 409 (:status dup-res)))
        (is (= :project-member-already-exists (get-in dup-res [:body :status])))
        (is (= 1 (count @project-member-atom)))))))


(deftest test-create-note
  (testing "Creating a note for a valid (organisation, project, user) should succeed."
    (let [req (get-new-request-for-project-note (factory/get-new-project-note))]
      (let [res (sut/post-create-note req)]
        (is (= 201 (:status res)))
        (is (= :project-note-created (get-in res [:body :status])))
        (is (= 1 (count @project-note-atom)))))))


(deftest test-create-note-invalid-org
  (testing "Creating a note for an invalid organisation should fail."
    (let [note (factory/get-new-project-note)
          req (assoc-in (get-new-request-for-project-note note) [:route-params :organisation-slug] "invalid-org-slug")]
      (let [res (sut/post-create-note req)]
        (is (= 500 (:status res)))
        (is (= :invalid-org-slug (get-in res [:body :status])))
        (is (= 0 (count @project-note-atom)))))))


(deftest test-create-note-invalid-project
  (testing "Creating a note for an invalid project should fail."
    (let [note (factory/get-new-project-note)
          req (assoc-in (get-new-request-for-project-note note) [:route-params :project-slug] invalid-uuid)]
      (let [res (sut/post-create-note req)]
        (is (= 400 (:status res)))
        (is (= :invalid-project-slug (get-in res [:body :status])))
        (is (= 0 (count @project-note-atom)))))))


(deftest test-create-note-duplicate
  (testing "Creating multiple notes with the same title and body should succeed."
    (let [note (factory/get-new-project-note)
          req (get-new-request-for-project-note note)]
      (let [res (sut/post-create-note req)]
        (is (= 201 (:status res)))
        (is (= :project-note-created (get-in res [:body :status])))
        (is (= 1 (count @project-note-atom))))
      (let [res (sut/post-create-note req)]
        (is (= 201 (:status res)))
        (is (= :project-note-created (get-in res [:body :status])))
        (is (= 2 (count @project-note-atom)))))))


(deftest test-edit-note
  (testing "Editing a note for a valid (organisation, project, user) should succeed."
    (let [note (factory/get-new-project-note)
          req (get-new-request-for-project-note note)]
      (let [res (sut/post-create-note req)]
        (is (= 201 (:status res)))
        (is (= :project-note-created (get-in res [:body :status])))
        (is (= 1 (count @project-note-atom)))
        (let [edit-req (assoc-in req [:form-params "title"] "updated title")
              edit-res (sut/put-edit-note edit-req)]
          (is (= 200 (:status edit-res)))
          (is (= :project-note-edited (get-in edit-res [:body :status])))
          (is (= 1 (count @project-note-atom)))
          (is (= "updated title" (-> @project-note-atom
                                     first
                                     :title))))))))


(deftest test-edit-note-invalid-org
  (testing "Editing a note for an invalid organisation should fail."
    (let [note (factory/get-new-project-note)
          req (assoc-in (get-new-request-for-project-note note) [:route-params :organisation-slug] "invalid-org-slug")]
      (let [res (sut/put-edit-note req)]
        (is (= 500 (:status res)))
        (is (= :invalid-org-slug (get-in res [:body :status])))
        (is (= 0 (count @project-note-atom)))))))


(deftest test-edit-note-invalid-project
  (testing "Editing a note for an invalid project should fail."
    (let [note (factory/get-new-project-note)
          req (assoc-in (get-new-request-for-project-note note) [:route-params :project-slug] invalid-uuid)]
      (let [res (sut/put-edit-note req)]
        (is (= 400 (:status res)))
        (is (= :invalid-project-slug (get-in res [:body :status])))
        (is (= 0 (count @project-note-atom)))))))

(deftest test-view-all-notes-authorized
  (testing "viewing all notes for a project for an authorized user should succeed."
    (with-redefs [proj/get-project-owner-or-member            (fn [project-id user-id]
                                                                (not= user-id invalid-uuid))
                  proj/get-all-notes-for-project-by-note-date (fn [note-date project-id]
                                                                (filter #(and (= project-id (str (:project_id %)))
                                                                              (= (fmt/parse (fmt/formatters :date) note-date)
                                                                                 (:note_date %)))
                                                                        @project-note-atom))]
      (let [project-id (factory/get-new-uuid)
            note       (-> (factory/get-new-project-note project-id)
                           (update-in [:project_id] str))
            notes-req  (replicate 10 (get-new-request-for-project-note note))]
        (doseq [note-req notes-req]
          (sut/post-create-note note-req))
        (let [note-project-id   (str (:project_id (first @project-note-atom)))
              get-note-req      (get-new-request-for-view-notes (assoc note :project_id note-project-id))
              res               (sut/get-all-notes get-note-req)]
          (is (= 10  (count (get-in res [:body :notes]))))
          (is (= 200 (:status res))))))))
