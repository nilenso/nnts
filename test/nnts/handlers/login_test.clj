(ns nnts.handlers.login-test
  (:require [nnts.handlers.login :as sut]
            [clojure.test :refer :all]
            [ring.middleware.json :refer (wrap-json-body)]
            [ring.mock.request :as mock]
            [nnts.config :as config]
            [nnts.db.user :as db-user]
            [nnts.auth.core :as auth]))

(defn setup [f]
  (reset! config/config-atom {:db {:classname   "org.postgresql.Driver"
                                   :subprotocol "postgresql"
                                   :subname  "//locahost:23001/nnts"
                                   :uri "postgresql://nnts:pwd@localhost:23001/nnts"
                                   :user "nnts"
                                   :password "pwd"}

                              :http-listen-port 9090
                              :google-auth {:server {:client-id "server_id"}
                                            :browser {:client-id "browser_id"}}})
  (f))

(use-fixtures :once setup)

(deftest post-login-test
  (testing "Should set token as the cookie when the token is passed"
    (let [db-user-create-calls (atom [])
          body {:id_token "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk",
                :google_user_id "106022680063408711019",
                :first_name "Test",
                :last_name "Subject",
                :photo_url "https://lh5.googleusercontent.com/photo.jpg"
                :email "test@nilenso.com"}
          login-req (-> (mock/request :post "/login")
                        (mock/json-body body))]
      (with-redefs [db-user/create-or-update-user (fn [_conn u] (swap! db-user-create-calls conj u))
                    auth/get-user-for-id-token (fn [_] {:google_user_id "106022680063408711019"
                                                        :first_name "Test"
                                                        :last_name "Subject"
                                                        :photo_url "https://lh5.googleusercontent.com/photo.jpg"
                                                        :email "test@nilenso.com"})]
        (let [decorated-handler (-> sut/post-login (wrap-json-body {:keywords? false}))
              response (decorated-handler login-req)]
          (is (= {:status 200
                  :headers {}
                  :body {:status "OK"}
                  :cookies {"session_id" {:value "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk" :http-only true :max-age 2592000}}}
                 response))
          (is (= (count @db-user-create-calls) 1))
          (is (= (first @db-user-create-calls) (dissoc body :id_token)))))))

  (testing "Should respond with a 401 and a json with status FAIL when the token is not passed"
    (let [login-req (mock/request :post "/login")]
      (is (= {:status 401
              :headers {}
              :body {:status "FAIL"}}
             ((-> sut/post-login (wrap-json-body {:keywords? false})) login-req))))))
