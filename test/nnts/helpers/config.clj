(ns nnts.helpers.config
  (:require [nnts.config :as config]
            [nnts.helpers.factory :as factory]))


(defn- get-server-protocol
  "Return the protcol of the current server instance."
  []
  "https")


(defn- get-server-port
  "Return the port number of the current server instance."
  []
  (config/http-listen-port))


(defn- get-server-address
  "Return the IP address of the current servcer instance."
  []
  "localhost") ;; TODO - fix this to read values from config


(defn get-server-base-url
  "Return the base URL of the current server instance."
  []
  (str (get-server-protocol)
       "://"
       (get-server-address)
       ":"
       (get-server-port)))
