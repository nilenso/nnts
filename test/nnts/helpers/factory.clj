;;;
;;; Module for factories to create test data for the testing modules.
;;;

(ns nnts.helpers.factory
  (:require [clj-time.core :as time]
            [clj-time.format :as format]))


(defn- current-date
  []
  (format/unparse (format/formatter "yyyy-MM-dd") (time/now)))


(defn- current-time
  []
  (format/unparse (format/formatter "yyyy-MM-dd:HH:mm:ss") (time/now)))

(defn get-new-uuid
  []
  (java.util.UUID/randomUUID))


(defn get-invalid-uuid
  []
  (new java.util.UUID 0 0))


(defn get-new-user
  "Return a new user entity."
  []
  {:google_user_id "test-google-id"
   :first_name "Test"
   :last_name "Subject"
   :photo_url "https://test.com/test-url"
   :email "test@test.com"
   :id (get-new-uuid)})


(defn get-new-organisation
  "Return a new test organisation entity."
  ([]
   (get-new-organisation (get-new-uuid)))
  ([owner-id]
   {:id (get-new-uuid)
    :name "test-org"
    :slug "test-slug"
    :owner_id owner-id}))


(defn get-new-project
  "Return a new project entity."
  ([]
   (get-new-project (get-new-uuid)))
  ([organisation-id]
   {:id (get-new-uuid)
    :name "test-proj"
    :description "test description"
    :slug "test-proj-slug"
    :organisation_id organisation-id
    }))


(defn get-new-project-member
  ([]
   (get-new-project-member (get-new-uuid) (get-new-uuid)))
  ([project-id]
   (get-new-project-member project-id (get-new-uuid)))
  ([project-id user-id]
   {:project_id project-id
    :user_id user-id}))


(defn- note
  [time date title body project-id user-id]
  {:note_date date
   :note_time_utc time
   :title title
   :body body
   :project_id project-id
   :user_id user-id})


(defn get-new-project-note
  ([]
   (note (current-time) (current-date) "note title" "note body" (get-new-uuid) (get-new-uuid)))
  ([project-id]
   (note (current-time) (current-date) "note title" "note body" project-id (get-new-uuid)))
  ([project-id user-id]
   (note (current-time) (current-date) "note title" "note body" project-id user-id)))
