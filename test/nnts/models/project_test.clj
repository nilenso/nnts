(ns nnts.models.project-test
  (:require [nnts.models.project :as sut]
            [nnts.db.db-helpers :as helpers]
            [nnts.db.helper :as db-helper]
            [nnts.helpers.factory :as factory]
            [nnts.db.project :as db-project]
            [clojure.test :refer :all]
            [failjure.core :as f]))


(use-fixtures
  :once
  (fn [test-fn]
    (helpers/migrate-db)
    (test-fn)))


(use-fixtures
  :each
  helpers/rollback-fixture)


(def invalid-date "0000-00-00")

(deftest test-create-project
  (testing "Should create a project for valid data."
    (let [test-project (factory/get-new-project)
          project-atom (atom [])]
      (let [project (factory/get-new-project)]
        (with-redefs [db-helper/get-conn (fn [] :create-proj-test-conn)
                      db-project/create-new-project (fn [conn {:keys [name organisation-id slug description]}]
                                                      (is (= conn :create-proj-test-conn))
                                                      (let [created-project {:name name
                                                                             :description description
                                                                             :organisation_id  organisation-id
                                                                             :slug slug}]
                                                        (do
                                                          (swap! project-atom conj created-project))
                                                        created-project))]
          (let [new-proj (sut/create-project test-project)]
            (is (= (count @project-atom) 1))))))))

(deftest test-add-project-member
  (testing "Should add a valid user to a valid project."
    (let [project-member (factory/get-new-project-member)
          project-member-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :add-member-to-project)
                    db-project/add-member-to-project (fn [conn member]
                                                       (is (= conn :add-member-to-project))
                                                       (if (contains? @project-member-atom member)
                                                         (f/fail (db-helper/construct-db-error :duplicate-project-member))
                                                         (do
                                                           (swap! project-member-atom conj member)
                                                           member)))]
        (let [newly-added-member (sut/add-member-to-project project-member)]
          (is (= (count @project-member-atom) 1))
          (is (= (first @project-member-atom) project-member)))))))


(deftest test-get-all-project-members
  (testing "Should retrieve the sequence of all project members."
    (let [project-id (factory/get-new-uuid)
          project-members (for [_ (range 10)] (factory/get-new-project-member project-id))
          project-member-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :get-all-project-members-conn)
                    db-project/add-member-to-project (fn [conn member]
                                                       (is (= conn :get-all-project-members-conn))
                                                       (if (contains? @project-member-atom member)
                                                         (f/fail (db-helper/construct-db-error :duplicate-project-member))
                                                         (do (swap! project-member-atom conj member)
                                                             member)))
                    db-project/get-all-users-by-project-id (fn [conn project-id]
                                                             (is (= conn :get-all-project-members-conn))
                                                             (filter #(= (:project_id %) project-id) @project-member-atom))]
        (f/attempt-all [_ (doall (map #(sut/add-member-to-project %) project-members))
                        all-members (sut/get-all-members-for-project project-id)]
                       (do
                         (is (= (count @project-member-atom) 10))
                         (is (= (set @project-member-atom)
                                (set all-members))))
                       (f/when-failed [{:keys [message] :as e}]
                         (println (:error message))))))))


(deftest test-add-project-duplicate-member
  (testing "Should not add a duplicate user to a valid project."
    (let [project-member (factory/get-new-project-member)
          project-member-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :add-member-to-project)
                    db-project/add-member-to-project (fn [conn member]
                                                       (is (= conn :add-member-to-project))
                                                       (if (contains? @project-member-atom member)
                                                         (f/fail (db-helper/construct-db-error :duplicate-project-member))
                                                         (do
                                                           (swap! project-member-atom conj member)
                                                           member)))]
        (let [newly-added-member (sut/add-member-to-project project-member)]
          (is (= (count @project-member-atom) 1))
          (is (= (first @project-member-atom) project-member)))
        (f/attempt-all [_ (sut/add-member-to-project project-member)]
                       (f/when-failed [{:keys [message] :as e}]
                         (is (= (:error message) :duplicate-project-member))))))))

(deftest test-create-note
  (testing "Should add a valid note to a valid project."
    (let [project-note (factory/get-new-project-note)
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :add-note-to-project)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :add-note-to-project))
                                                 (swap! project-note-atom conj note))]
        (let [newly-added-note (sut/create-note project-note)]
          (is (= (count @project-note-atom) 1))
          (is (= (first @project-note-atom) project-note)))))))


(deftest test-get-all-project-notes
  (testing "Should retrieve the sequence of all notes for this project."
    (let [project-id (factory/get-new-uuid)
          project-notes (for [_ (range 10)] (factory/get-new-project-note project-id))
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :get-all-project-notes-conn)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :get-all-project-notes-conn))
                                                 (swap! project-note-atom conj note))
                    db-project/get-all-notes-by-project-id (fn [conn project-id]
                                                             (is (= conn :get-all-project-notes-conn))
                                                             (filter #(= (:project_id %) project-id) @project-note-atom))]
        (f/attempt-all [_ (doall (map #(sut/create-note %) project-notes))
                        all-notes (sut/get-all-notes-for-project project-id)]
                       (do
                         (is (= (count @project-note-atom) 10))
                         (is (= (set @project-note-atom)
                                (set all-notes))))
                       (f/when-failed [{:keys [message] :as e}]
                         (println (:error message))))))))


(deftest test-add-project-duplicate-note
  (testing "Should be able to add a duplicate note to a valid project."
    (let [project-note (factory/get-new-project-note)
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :add-note-to-project)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :add-note-to-project))
                                                 (swap! project-note-atom conj note))]
        (let [_ (sut/create-note project-note)]
          (is (= 1 (count @project-note-atom)))
          (is (= (first @project-note-atom) project-note)))
        (let [_ (sut/create-note project-note)]
          (is (= 2 (count @project-note-atom)))
          (is (= (first @project-note-atom) project-note)))))))


(deftest test-edit-note
  (testing "Should edit existing note."
    (let [project-note (factory/get-new-project-note)
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :add-note-to-project)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :add-note-to-project))
                                                 (swap! project-note-atom conj note))
                    db-project/edit-note (fn [_conn _id note]
                                           (reset! project-note-atom [note]))]
        (let [newly-added-note (first (sut/create-note project-note))
              updated-note (assoc newly-added-note
                                  :title "updated title"
                                  :body "updated body")
              _ (sut/edit-note (factory/get-new-uuid) updated-note)]
          (is (= updated-note (first @project-note-atom))))))))


(deftest test-view-all-notes-valid
  (testing "Should be able to view all notes from project, if valid."
    (let [project-note (factory/get-new-project-note)
          project-notes (replicate 10 project-note)
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :get-all-notes-by-date)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :get-all-notes-by-date))
                                                 (swap! project-note-atom conj note))
                    db-project/get-all-notes-by-note-date-and-project-id (fn [conn note-date project-id]
                                                                           (is (= conn :get-all-notes-by-date))
                                                                           (filter #(and (= project-id (:project_id %))
                                                                                         (= note-date (:note_date %)))
                                                                                   @project-note-atom))]
        (doall (map #(sut/create-note %) project-notes))
        (let [notes (sut/get-all-notes-for-project-by-note-date (:note_date project-note)
                                                                (:project_id project-note))]
          (is (= 10 (count notes))))))))


(deftest test-view-all-notes-invalid
  (testing "Should not be able to view all notes from project, if invalid."
        (let [project-note (factory/get-new-project-note)
          project-notes (replicate 10 project-note)
          project-note-atom (atom [])]
      (with-redefs [db-helper/get-conn (fn [] :get-all-notes-by-date)
                    db-project/create-new-note (fn [conn note]
                                                 (is (= conn :get-all-notes-by-date))
                                                 (swap! project-note-atom conj note))
                    db-project/get-all-notes-by-note-date-and-project-id (fn [conn note-date project-id]
                                                                           (is (= conn :get-all-notes-by-date))
                                                                           (filter #(and (= project-id (:project_id %))
                                                                                         (= note-date (:note_date %)))
                                                                                   @project-note-atom))]
        (doall (map #(sut/create-note %) project-notes))
        (let [notes (sut/get-all-notes-for-project-by-note-date (:note_date invalid-date)
                                                                (:project_id project-note))]
          (is (= 0 (count notes))))))))
