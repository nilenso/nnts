(ns nnts.models.organisation-test
  (:require [nnts.models.organisation :as sut]
            [nnts.db.organisation :as db-org]
            [nnts.db.core :as db-core]
            [nnts.db.helper :as db-helper]
            [nnts.helpers.factory :as factory]
            [clojure.test :refer :all]
            [nnts.db.db-helpers :as helpers]
            [nnts.db.helper :as helper]))


(use-fixtures
  :once
  (fn [test-fn]
    (helpers/migrate-db)
    (test-fn)))


(use-fixtures
  :each
  helpers/rollback-fixture)


(deftest create-organisation-test
  (testing "Should create an organisation for valid data."
    (let [org-atom (atom [])]
      (let [org (factory/get-new-organisation)]
        (with-redefs [db-helper/get-conn (fn [] :create-org-test-conn)
                      db-org/create-new-organisation (fn [conn org]
                                                       (is (= conn :create-org-test-conn))
                                                       (do
                                                         (swap! org-atom conj org)))]
          (let [new-org (sut/create-organisation org)]
            (is (= (count @org-atom) 1))
            (is (= (last @org-atom) org))))))))
