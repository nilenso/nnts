(ns nnts.models.user-test
  (:require [nnts.models.user :as sut]
            [nnts.db.user :as db-user]
            [nnts.db.core :as db-core]
            [clojure.test :refer :all]
            [nnts.db.db-helpers :as helpers]))


(use-fixtures
 :once
  (fn [test-fn]
    (helpers/migrate-db)
    (test-fn)))


(use-fixtures
  :each
  helpers/rollback-fixture)


(def test-user {:google_user_id "106022680063408711019"
                :first_name "Test"
                :last_name "Subject"
                :photo_url "https://lh5.googleusercontent.com/photo.jpg"
                :email "test@nilenso.com"})


(deftest create-user-test
  (testing "Should create a user for valid data."
    (let [user-atom (atom [])]
      (with-redefs [db-core/insert-row (fn [_conn _table orgs]
                                         (swap! user-atom conj test-user))
                    db-user/get-user-id-for-google-user-id
                    (fn [_conn _google_user_id]
                      @user-atom)]
        (sut/create-user test-user)
        (is (= (-> (db-user/get-user-id-for-google-user-id helpers/*txn* "106022680063408711019")
                   first)
               test-user))))))
