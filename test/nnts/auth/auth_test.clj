(ns nnts.auth.auth-test
  (:require [nnts.auth.core :as auth]
            [nnts.config :as config]
            [nnts.handlers.login :as sut]
            [clojure.test :refer :all]
            [clj-http.client :as client]
            [clojure.data.json :as json]))

  (def escaped-success-body-string  "{ \"iss\": \"accounts.google.com\", \"azp\": \"377161506506-bed7k41vncf6fcrviv65kgev45pn44m6.apps.googleusercontent.com\", \"aud\": \"377161506506-bed7k41vncf6fcrviv65kgev45pn44m6.apps.googleusercontent.com\", \"sub\": \"115868663056647332175\", \"email\": \"timmy@nilenso.com\", \"email_verified\": \"true\", \"at_hash\": \"-3Thgcqy4nIdeKYs9C1Aqw\", \"name\": \"Timmy Jose\", \"picture\": \"https://lh4.googleusercontent.com/photo.jpg\", \"given_name\": \"Timmy\", \"family_name\": \"Jose\", \"locale\": \"en\", \"iat\": \"1539936905\", \"exp\": \"1539940505\", \"jti\": \"3f1973e70510df1eac628dbe6939b92373fd51c9\", \"alg\": \"RS256\", \"kid\": \"961cf60bcedd9067c4cf1f2ddf4ed612b536fb1a\", \"typ\": \"JWT\"}")

(def escaped-failure-body-string  "{ \"iss\": \"accounts.google.com\", \"azp\": \"377161506506-bed7k41vncf6fcrviv65kgev45pn44m6.apps.googleusercontent.com\", \"aud\": \"161506506-bed7k41vncf6fcrviv65kgev45pn44m6.apps.googleusercontent.com\", \"sub\": \"115868663056647332175\", \"email\": \"timmy@nilenso.com\", \"email_verified\": \"true\", \"at_hash\": \"-3Thgcqy4nIdeKYs9C1Aqw\", \"name\": \"Timmy Jose\", \"picture\": \"https://lh4.googleusercontent.com/photo.jpg\", \"given_name\": \"Timmy\", \"family_name\": \"Jose\", \"locale\": \"en\", \"iat\": \"1539936905\", \"exp\": \"1539940505\", \"jti\": \"3f1973e70510df1eac628dbe6939b92373fd51c9\", \"alg\": \"RS256\", \"kid\": \"961cf60bcedd9067c4cf1f2ddf4ed612b536fb1a\", \"typ\": \"JWT\"}")


(defn setup [f]
  (reset! config/config-atom { :google-auth {:browser { :client-id "377161506506-bed7k41vncf6fcrviv65kgev45pn44m6"}}})
  (f))

(use-fixtures :once setup)

(deftest post-login-auth-test
  (testing "should return the user object when the id token has been verified"
    (with-redefs [client/get (fn [_] {:body escaped-success-body-string})]
      (let [id-token "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk"
            auth-user (auth/get-user-for-id-token id-token)]
        (is (=  {:google_user_id "115868663056647332175"
                 :first_name "Timmy"
                 :last_name "Jose"
                 :photo_url "https://lh4.googleusercontent.com/photo.jpg"
                 :email "timmy@nilenso.com"}
                auth-user)))))

  (testing "should return nil in the case of an error"
    (with-redefs [client/get (fn [_] {:body escaped-failure-body-string})]
      (let [id-token "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijk"
            auth-user (auth/get-user-for-id-token id-token)]
        (is (nil? auth-user))))))
