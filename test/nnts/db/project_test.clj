(ns nnts.db.project-test
  (:require [nnts.db.project :as sut]
            [nnts.helpers.factory :as factory]
            [nnts.db.db-helpers :as db-helpers]
            [nnts.handlers.helper :as resp-helper]
            [clojure.test :refer :all]
            [nnts.db.core :as db-core]
            [nnts.db.user :as db-user]
            [nnts.db.helper :as helper]
            [nnts.db.organisation :as db-org]
            [failjure.core :as f]
            [nnts.db.helper :as db-helper]
            [nnts.db.project :as db-project]))


;; run the migration once

(use-fixtures
  :once
  (fn [test-fn]
    (db-helpers/migrate-db)
    (test-fn)))


;; make sure to run the actual tests inside transactions

(use-fixtures
  :each
  db-helpers/rollback-fixture)


(deftest test-create-project
  (testing "Create project should succeed for valid data."
    (let [user (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id user))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))]
      (db-user/create-or-update-user db-helpers/*txn* user)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (is (= (sut/get-project-by-name-and-organisation-id db-helpers/*txn* (:name project) (:organisation_id project))
             project)))))


(deftest test-create-project-invalid-organisation
  (testing "Create project should fail for non-existent organisation."
    (let [expected-failure (helper/construct-db-error :organisation-for-new-project-does-not-exist)]
      (is (=  expected-failure
              (f/message (sut/create-new-project db-helpers/*txn* (factory/get-new-project))))))))


(deftest test-create-project-duplicate-project
  (testing "Create project should fail for duplicate project."
    (let [user (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id user))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))
          expected-failure (helper/construct-db-error :duplicate-project)]
      (db-user/create-or-update-user db-helpers/*txn* user)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (is (= expected-failure
             (f/message (sut/create-new-project db-helpers/*txn* project)))))))


(deftest test-add-member-to-project
  (testing "Adding a valid user to a valid project should succeed."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))
          member (assoc (factory/get-new-user) :google_user_id "test-google-user-id-1")]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-user/create-or-update-user db-helpers/*txn* member)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [project-member (factory/get-new-project-member (:id project) (:id member))]
        (sut/add-member-to-project db-helpers/*txn* project-member)
        (let [members (sut/get-all-users-by-project-id db-helpers/*txn* (:id project))]
          (is (= 1 (count members)))
          (is (= project-member (first members))))))))


(deftest test-add-member-to-project-duplicate
  (testing "Adding a valid user to a valid project more than once should fail."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))
          member (assoc (factory/get-new-user) :google_user_id "test-google-user-id-1")]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-user/create-or-update-user db-helpers/*txn* member)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [project-member (factory/get-new-project-member (:id project) (:id member))]
        (sut/add-member-to-project db-helpers/*txn* project-member)
        (f/attempt-all [_ (sut/add-member-to-project db-helpers/*txn* project-member)]
                       (f/when-failed [{:keys [message] :as e}]
                         (is (= (:error message) :duplicate-project-member))))))))


(deftest test-create-note-for-project
  (testing "Adding a valid note to a valid project should succeed."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [project-note (factory/get-new-project-note (:id project) (:id owner))]
        (sut/create-new-note db-helpers/*txn* project-note)
        (let [notes (sut/get-all-notes-by-project-id db-helpers/*txn* (:id project))]
          (is (= 1 (count notes)))
          (is (= (dissoc project-note :note_date :note_time_utc)
                 (-> notes
                     first
                     (dissoc :id)
                     (dissoc :note_date)
                     (dissoc :note_time_utc)))))))))


(deftest test-create-duplicate-note-for-project
  (testing "Adding a valid note to a valid project more than once should succeed."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [project-notes (replicate 10 (factory/get-new-project-note (:id project) (:id owner)))]
        (doall (map #(sut/create-new-note db-helpers/*txn* %) project-notes))
        (let [notes (sut/get-all-notes-by-project-id db-helpers/*txn* (:id project))]
          (is (= 10 (count notes))))))))


(deftest test-view-all-notes-by-date-valid
  (testing "Viewing all the notes of a project by note date should succeed."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [note (factory/get-new-project-note (:id project) (:id owner))
            project-notes (replicate 10 note)]
        (doall (map #(sut/create-new-note db-helpers/*txn* %) project-notes))
        (let [notes (sut/get-all-notes-by-note-date-and-project-id db-helpers/*txn* (:note_date note) (str (:id project)))]
          (is (= 10 (count notes))))))))


(deftest test-view-all-notes-by-date-invalid
  (testing "Viewing all the notes of a project by invalid note date should fail."
    (let [owner (factory/get-new-user)
          organisation (assoc (factory/get-new-organisation) :owner_id (:id owner))
          project (assoc (factory/get-new-project) :organisation_id (:id organisation))]
      (db-user/create-or-update-user db-helpers/*txn* owner)
      (db-org/create-new-organisation db-helpers/*txn* organisation)
      (sut/create-new-project db-helpers/*txn* project)
      (let [note (factory/get-new-project-note (:id project) (:id owner))
            project-notes (replicate 10 note)]
        (doall (map #(sut/create-new-note db-helpers/*txn* %) project-notes))
        (let [notes (sut/get-all-notes-by-note-date-and-project-id db-helpers/*txn* "0000-00-00" (str (:id project)))]
          (is (= 0 (count notes))))))))
