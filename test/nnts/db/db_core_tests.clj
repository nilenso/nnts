(ns nnts.db.db-core-tests
  (:require  [clojure.test :refer :all]
             [migratus.core :as migratus]
             [nnts.config :as config]
             [nnts.db.core :as sut]
             [nnts.db.db-helpers :as helpers]
             [nnts.db.core :as db-core]))


;; run the migration once

(use-fixtures
  :once
  (fn [test-fn]
    (helpers/migrate-db)
    (test-fn)))

;; make sure to run the actual tests inside transactions

(use-fixtures
  :each
  helpers/rollback-fixture)

;;
;; test data
;;

(def test-user {:id (java.util.UUID/randomUUID)
                :google_user_id "test-google-id-1"
                :first_name "Test1"
                :last_name "Subject"
                :photo_url "https://test.com/test1-url"
                :email "test1@test.com"})


(def test-org {:name "test-org-1"
               :slug "test-slug-1"})


;;; add tests for CRUD ops on users and orgs (for now).
;;; Not adding more tests since this module will most likely
;;; be replaced with honeysql. Comprehensive tests will be
;;; written then.

(deftest all-in-one-test
  (testing "Adding a user to the users table should work."
    (sut/insert-row helpers/*txn* :users test-user)
    (let [stored-user (-> (sut/retrieve-all-rows helpers/*txn* "users")
                          first)]
      (is (= stored-user
             test-user))))
  (testing "Adding an organisation to the organisations table should work."
    (let [stored-user-id (-> (sut/retrieve-all-rows helpers/*txn* "users")
                             first
                             :id)
          decorated-org (assoc test-org :owner_id stored-user-id)]
      (sut/insert-row helpers/*txn* :organisations decorated-org)
      (is (= (-> (sut/retrieve-all-rows helpers/*txn* "organisations")
                 first
                 (dissoc :id :owner_id))
             test-org)))))


(deftest test-update-row-by-id
  (testing "Updating a user's details should work."
    (sut/insert-row helpers/*txn* :users test-user)
    (let [updated-user (assoc test-user :first_name "first_name")]
      (is (= 1 (count (sut/update-row-by-id helpers/*txn* "users" (:id test-user) updated-user))))
      (let [user (-> (sut/retrieve-all-rows-where helpers/*txn* "users" "id" (:id test-user))
                     first)]
        (is (= user updated-user))))))
