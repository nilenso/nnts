(ns nnts.db.user-test
  (:require [nnts.db.user :as sut]
            [nnts.db.helper :as db-helper]
            [nnts.db.core :as db-core]
            [clojure.test :refer :all]))



(def test-user {:id "test-id"
                :google_user_id "test-google-id"
                :first_name "Test"
                :last_name "Subject"
                :photo_url "https://test.com/test-url"
                :email "test@test.com"})


(def user-atom (atom nil))


(deftest create-or-update-test
  (testing "Should create a user when it does not exist"
    (with-redefs [db-core/is-row-present (fn [_conn _google-user-id id _table]
                                           (when @user-atom
                                             (get @user-atom id)))
                  db-core/insert-row (fn [_conn _google-user-id user]
                                       (reset! user-atom {(:google_user_id user) user}))
                  db-core/update-row (fn [_conn _google-user-id id user _table]
                                       (update-in @user-atom [(:google_user_id user)] assoc :google_user_id (:google_user_id user)
                                                  :first_name (:first_name user) :last_name (:last_name user)
                                                  :photo_url (:photo_url user) :email (:email user)))
                  db-core/retrieve-all-rows-where (fn [_conn _table _key _google-user-id]
                                                    (when-let [user (get @user-atom _google-user-id)]
                                                      (list user)))]
      (sut/create-or-update-user (db-helper/get-conn) test-user)
      (is (= (sut/get-user-id-for-google-user-id (db-helper/get-conn) (:google_user_id test-user))
             (:id test-user))))))
