(ns nnts.db.organisation-test
  (:require [nnts.db.organisation :as sut]
            [nnts.db.user :as db-user]
            [nnts.db.core :as db-core]
            [nnts.db.db-helpers :as helpers]
            [nnts.helpers.factory :as factory]
            [nnts.config :as config]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer :all]))


;; run the migration once

(use-fixtures
  :once
  (fn [test-fn]
    (helpers/migrate-db)
    (test-fn)))


;; make sure to run the actual tests inside transactions

(use-fixtures
  :each
  helpers/rollback-fixture)


(deftest test-create-new-organisation
  (testing "Adding a valid organisation entry should succeed."
    (let [user (factory/get-new-user)
          org (assoc (factory/get-new-organisation) :owner_id (:id user))]
      (db-user/create-or-update-user helpers/*txn* user)
      (sut/create-new-organisation helpers/*txn* org)
      (let [created-org (sut/get-organisation-by-slug helpers/*txn* (:slug org))]
        (is (= created-org
               org))))))






