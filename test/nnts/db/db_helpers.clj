;;;
;;; Common fixtures for all db tests.
;;;
(ns nnts.db.db-helpers
  (:require [clojure.test :refer :all]
            [clojure.java.jdbc :as jdbc]
            [nnts.config :as config]
            [nnts.db.core :as core]
            [migratus.core :as migratus]))


(defn migrate-db
  []
  (config/load-config)
  (let [db-conf (config/migration-config)]
    (migratus/init db-conf)
    (migratus/migrate db-conf)))


(declare ^:dynamic *txn*)


(defn rollback-fixture
  [test-fn]
  (let [db-spec (config/db-uri)]
    (jdbc/with-db-transaction [txn db-spec]
      (jdbc/db-set-rollback-only! txn)
      (binding [*txn* txn]
        (test-fn)))))
