CREATE TABLE project_members (
    project_id UUID NOT NULL REFERENCES projects(id),
    user_id UUID NOT NULL REFERENCES users(id),
    UNIQUE (project_id, user_id)
);

CREATE UNIQUE INDEX IF NOT EXISTS projects_by_project_id_and_user_id on project_members (project_id, user_id);

