CREATE TABLE project_notes (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    title text NOT NULL,
    body text NOT NULL,
    project_id UUID REFERENCES projects(id),
    user_id UUID REFERENCES users(id),
    note_date TIMESTAMP
);

CREATE INDEX IF NOT EXISTS project_notes_by_project_and_user_id ON project_notes (project_id, user_id);

