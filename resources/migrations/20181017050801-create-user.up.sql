CREATE TABLE IF NOT EXISTS users (
       id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
       google_user_id TEXT NOT NULL,
       first_name TEXT,
       last_name TEXT,
       email TEXT,
       photo_url TEXT
);

CREATE UNIQUE INDEX IF NOT EXISTS users_by_google_user_id ON users (google_user_id);
