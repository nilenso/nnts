CREATE TABLE IF NOT EXISTS organisations (
      id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
      name TEXT NOT NULL,
      slug TEXT UNIQUE NOT NULL,
      owner_id UUID REFERENCES users(id)
);

CREATE UNIQUE INDEX IF NOT EXISTS orgs_by_name ON organisations (name);

