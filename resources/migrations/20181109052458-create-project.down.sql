DROP TABLE IF EXISTS projects;

DROP INDEX IF EXISTS projects_by_name;
DROP INDEX IF EXISTS projects_by_slug;
