CREATE TABLE projects (
       id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
       name text NOT NULL,
       description text,
       slug text NOT NULL,
       organisation_id UUID REFERENCES organisations(id),
       UNIQUE (name, organisation_id),
       UNIQUE (slug, organisation_id)
);

CREATE UNIQUE INDEX IF NOT EXISTS projects_by_name on projects (name, organisation_id);
CREATE UNIQUE INDEX IF NOT EXISTS projects_by_slug on projects (slug, organisation_id);
       
