(ns nnts.helpers.core)

(defn string->uuid
  [uuid-str]
  (java.util.UUID/fromString uuid-str))
