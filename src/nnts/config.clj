(ns nnts.config
  (:require [aero.core :refer (read-config)]))


(def config-atom (atom {}))

(defn load-config []
  (reset! config-atom (read-config (clojure.java.io/resource "config.edn"))))

(defn http-listen-port []
  (:http-listen-port @config-atom))

(defn nrepl-port []
  (Integer/parseInt (:nrepl-port @config-atom)))

(defn google-auth-client-id [resource]
  (get-in @config-atom [:google-auth resource :client-id]))

(defn google-auth-tokeninfo-endpoint []
  (get @config-atom :google-auth-tokeninfo-endpoint))

(defn db-uri []
  (let [{:keys [uri user password host port name]} (:db @config-atom)]
    (or (not-empty uri)
        (format "postgresql://%s:%s@%s:%s/%s"
                user password host port name))))

(defn migration-config []
  (let [db-config (:db @config-atom)]
    {:store                :database
     :migration-dir        "migrations/"
     :migration-table-name "migrations"
     :init-script          "init.sql"
     :db                   (-> db-config
                               (assoc :uri (db-uri))
                               (dissoc :host :port :name))}))
