(ns nnts.handlers.helper
  (:require [ring.util.response :as res]
            [clj-time.format :as f]))


(defn create-response
  "Helper function to create HTTP responses."
  [status-string return-code & human-readabla-message]
  (let [response (-> (res/response {:status status-string})
                     (res/status return-code))]
    (if human-readabla-message
      (assoc-in response [:body :description] (first human-readabla-message))
      response)))

(defn construct-handler-error
  [error human-readable-message]
  {:type :handler-error
   :error error
   :human-readable-message human-readable-message})
