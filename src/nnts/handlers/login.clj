(ns nnts.handlers.login
  (:require [ring.util.response :as res]
            [selmer.parser :as selmer]
            [nnts.auth.core :as auth]
            [nnts.config :as config]
            [nnts.models.user :as user]))


(def ^:private session-max-age (* 30 24 60 60))


(defn get-login [req]
  (res/response (selmer/render-file "login.html"
                                    {:google-auth-client-id
                                     (config/google-auth-client-id :browser)})))


(defn post-failure-response []
  (-> (res/response {:status "FAIL"})
      (res/status 401)))


(defn post-login [{:keys [body] :as req}]
  (if-let [token (get body "id_token")]
    (if-let [auth-user (auth/get-user-for-id-token token)]
      (do
        (user/create-user auth-user)
        (-> (res/response {:status "OK"})
            (assoc :cookies {"session_id" {:value token :http-only true :max-age session-max-age}})))
      (post-failure-response))
    (post-failure-response)))
