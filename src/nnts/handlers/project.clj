(ns nnts.handlers.project
  (:require [clojure.spec.alpha :as s]
            [clojure.walk :refer [keywordize-keys]]
            [ring.util.response :as res]
            [failjure.core :as f]
            [nnts.auth.core :as auth]
            [nnts.models.organisation :as org]
            [nnts.models.user :as user]
            [nnts.handlers.helper :as helper]
            [nnts.helpers.core :as ch]
            [nnts.models.project :as proj]
            [clj-time.format :as format]))


(defn get-http-status-code-for-error-kind
  [kind]
  (case kind
    :project-member-already-exists        409
    :project-already-exists               409
    :slug-does-not-match-any-organisation 400
    :invalid-project-slug                 400
    :validation-failed-user-not-found     400
    :user-not-owner-of-organisation       400
    :project-data-invalid                 400
    :project-member-does-not-exist        400
    :failed-to-create-note                500
    500))


(defn- extract-project-data
  "Select and return values for the keys of interest; returns a map."
  [form-params org-id]
  (-> (select-keys form-params ["name" "description" "slug"])
      (keywordize-keys)
      (assoc :organisation_id org-id)))


(defn- create-project
  "Helper function to create the org; return nil if any step fails."
  [form-params valid-org-id]
  (let [project-data (extract-project-data form-params valid-org-id)]
    (let [valid-project-data (s/conform :nnts.models.project/project project-data)]
      (if (= :clojure.spec.alpha/invalid valid-project-data)
        (f/fail (helper/construct-handler-error :project-data-invalid-format
                                                (format "Project data %s is invalid or in invallid format" project-data)))
        (proj/create-project valid-project-data)))))


(defn post-create-project
  "Dispatch the project data to the project model for further processing."
  [{:keys [form-params route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)]
    (f/attempt-all [valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)]
                   (do
                     (f/attempt-all [_ (create-project form-params valid-organisation-id)]
                                    (helper/create-response :project-create-success 201)
                                    (f/when-failed [e]
                                      (f/fail (f/message e)))))
                   (f/when-failed [{:keys [message] :as e}]
                     (let [error (:error message)]
                       (helper/create-response error
                                               (get-http-status-code-for-error-kind error)
                                               (:human-readable-message message)))))))

(defn get-list-members
  "Retrieve the list of all users for the (organisation, project) combination."
  [{:keys [route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)
        project-slug      (:project-slug route-params)]
    (f/attempt-all [valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)
                    valid-project-id (proj/validate-project-by-slug
                                      project-slug
                                      valid-organisation-id)]
                   (do
                     (f/attempt-all [members (proj/get-all-members-for-project valid-project-id)]
                                    (-> (helper/create-response "Ok" 200)
                                        (assoc-in [:body :members] members))
                                    (f/when-failed [e]
                                      (f/fail (f/message e)))))
                   (f/when-failed [{:keys [message] :as e}]
                     (let [error (:error message)]
                       (helper/create-response error
                                               (get-http-status-code-for-error-kind error)
                                               (:human-readable-message message)))))))


(defn- create-project-member
  "Helper function to create the project_member."
  [project-id user-id]
  {:project_id project-id
   :user_id    user-id})


;;
;; POST for create member
;;


(defn post-create-member
  "Dispatch the user and (organisation, project) information to the model to
  add user as a new member of the project."
  [{:keys [form-params route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)
        project-slug      (:project-slug route-params)
        user-id           (-> (get form-params "user-id")
                              ch/string->uuid)]
    (f/attempt-all [valid-user-id (user/validate-user-id user-id)
                    valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)
                    valid-project-id (proj/validate-project-by-slug
                                      project-slug
                                      valid-organisation-id)]
                   (f/attempt-all [_ (proj/add-member-to-project (create-project-member valid-project-id
                                                                                        valid-user-id))]
                                  (helper/create-response :project-member-add-success 201)
                                  (f/when-failed [e]
                                    (f/fail (f/message e))))
                   (f/when-failed [{:keys [message] :as e}]
                     (let [error (:error message)]
                       (helper/create-response error
                                               (get-http-status-code-for-error-kind error)
                                               (:human-readable-message message)))))))


(defn get-list-notes
  "Retrieve all the notes for the (organisation, project) combination"
  [{:keys [form-params route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)
        project-slug      (:project-slug route-params)]
    (f/attempt-all [valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)
                    valid-project-id (proj/validate-project-by-slug
                                      project-slug
                                      valid-organisation-id)]
                   (f/attempt-all [notes (proj/get-all-notes-for-project valid-project-id)]
                                  (-> (helper/create-response "Ok" 200)
                                      (assoc-in [:body :notes] notes))
                                  (f/when-failed [e]
                                    (f/fail (f/message e))))
                   (f/when-failed [{:keys [message] :as e}]
                     (let [error (:error message)]
                       (helper/create-response error
                                               (get-http-status-code-for-error-kind error)
                                               (:human-readable-message error)))))))


(def custom-date-formatter (format/formatter "yyyy-MM-dd"))

(defn- note
  [date title body project-id user-id]
  {:note_date  (format/parse custom-date-formatter date)
   :title      title
   :body       body
   :project_id project-id
   :user_id    user-id})


(defn- create-error-response
  [message]
  (let [error (:error message)]
    (helper/create-response error
                            (get-http-status-code-for-error-kind error)
                            (:human-readable-message message))))


(defn post-create-note
  "Create the given note for the (organisation, project, logged_in_user) combination."
  [{:keys [form-params route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)
        project-slug      (:project-slug route-params)
        note-date         (get form-params "note_date")
        note-title        (get form-params "title")
        note-body         (get form-params "body")]
    (f/attempt-all [valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)
                    valid-project-id (proj/validate-project-by-slug
                                      project-slug
                                      valid-organisation-id)
                    valid-user-id (proj/get-project-owner-or-member
                                   valid-project-id
                                   authenticated-user-id)]
                   (f/attempt-all [new-note (proj/create-note (note note-date
                                                                    note-title
                                                                    note-body
                                                                    valid-project-id
                                                                    valid-user-id))]
                                  (helper/create-response :project-note-created 201)
                                  (f/when-failed [e]
                                    (f/fail (f/message e))))
                   (f/when-failed [{:keys [message] :as e}]
                     (create-error-response message)))))


(defn put-edit-note
  "Edit the given note for the (organisation, project, logged_in_user) combination."
  [{:keys [form-params route-params authenticated-user-id] :as req}]
  (let [organisation-slug (:organisation-slug route-params)
        project-slug      (:project-slug route-params)
        note-id           (:note-id route-params)
        note-date         (get form-params "note_date")
        note-title        (get form-params "title")
        note-body         (get form-params "body")]
    (f/attempt-all [valid-organisation-id (org/validate-organisation-by-slug
                                           organisation-slug
                                           authenticated-user-id)
                    valid-project-id (proj/validate-project-by-slug
                                      project-slug
                                      valid-organisation-id)
                    valid-user-id (proj/get-project-owner-or-member
                                   valid-project-id
                                   authenticated-user-id)]
                   (f/attempt-all [new-note (proj/edit-note note-id (note  note-date
                                                                           note-title
                                                                           note-body
                                                                           valid-project-id
                                                                           valid-user-id))]
                                  (helper/create-response :project-note-edited 200)
                                  (f/when-failed [e]
                                    (f/fail (f/message e))))
                   (f/when-failed [{:keys [message] :as e}]
                     (create-error-response message)))))

(defn validate-note-date
  [date]
  (s/valid? :nnts.models.project/date date))

(defn get-all-notes
  "Retrieve all notes of all users for the given project."
  [{:keys [query-params authenticated-user-id] :as req}]
  (let [{:strs [note-date project-id]} query-params]
    (if-not (validate-note-date note-date)
      (helper/create-response :invalid-note-date 400)
      (f/attempt-all [_ (proj/get-project-owner-or-member
                         (ch/string->uuid project-id)
                         authenticated-user-id)
                      notes (proj/get-all-notes-for-project-by-note-date note-date
                                                                         project-id)]
                     (-> (helper/create-response "Ok" 200)
                         (assoc-in [:body :notes] notes))
                     (f/when-failed [{:keys [message] :as e}]
                       (create-error-response message))))))
