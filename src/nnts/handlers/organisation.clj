(ns nnts.handlers.organisation
  (:require [clojure.spec.alpha :as s]
            [clojure.walk :refer [keywordize-keys]]
            [ring.util.response :as res]
            [selmer.parser :as selmer]
            [nnts.auth.core :as auth]
            [nnts.handlers.helper :as helper]
            [nnts.models.organisation :as org]))

;;
;; GET
;;
(defn get-create-organisation
  "The organisations creation page."
  [{:keys [authenticated-user-id] :as req}]
  (if authenticated-user-id
    (res/response (selmer/render-file "create-org.html" {}))
    (helper/create-response "ORG-CREATE-FAIL-NO-AUTH")))


(defn- extract-org-data
  "Select and return values for the keys of interest; returns a map."
  [form-params user-id]
  (-> (select-keys form-params ["name" "slug"])
      (keywordize-keys)
      (assoc :owner_id user-id)))


(defn- create-organisation
  "Helper function to create the org; return nil if any step fails."
  [form-params authenticated-user-id]
  (some->> (extract-org-data form-params authenticated-user-id)
          (s/conform :nnts.models.organisation/organisation)
          (org/create-organisation)))

;;
;; POST
;;
(defn post-create-organisation
  "Dispatch the organisation data to the organisation model for further processing."
  [{:keys [form-params authenticated-user-id] :as req}]
  (if authenticated-user-id
    (if (create-organisation form-params authenticated-user-id)
      (helper/create-response "ORG-CREATE-SUCCESS" 200)
      (helper/create-response "ORG-CREATE-FAIL" 500))
    (helper/create-response "ORG-CREATE-FAIL-NO-AUTH" 401)))
