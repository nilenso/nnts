(ns nnts.models.organisation
  (:require [clojure.spec.alpha :as s]
            [failjure.core :as f]
            [nnts.models.helper :as helper]
            [nnts.db.user :as db-user]
            [nnts.config :as config]
            [nnts.db.helper :as db-helper]
            [nnts.db.organisation :as db-org]))


;; specS for valid organisation data

(s/def ::name (s/and string? not-empty))
(s/def ::slug (s/and string? not-empty))
(s/def ::owner_id uuid?)


;; create spec for project structure
(s/def ::organisation (s/keys :req-un [::name ::slug ::owner_id]))


(defn create-organisation
  "Insert the organisation into the organisations table."
  [org]
  (db-org/create-new-organisation (db-helper/get-conn) org))


(defn get-organisation-id-by-slug
  "Retrieve an organisation's id by slug if present else nil."
  [org-slug]
  (db-org/get-organisation-id-by-slug (db-helper/get-conn) org-slug))


(defn validate-organisation-by-slug
  "Check to make sure that the organisation exists, and that the logged-in user
  is the same as the owner of the organisation."
  [organisation-slug logged-user-id]
  (f/attempt-all [valid-organisation (db-org/get-organisation-by-slug (db-helper/get-conn)
                                                                      organisation-slug)]
                 (if (= logged-user-id (:owner_id valid-organisation))
                   (:id valid-organisation)
                   (f/fail (helper/construct-model-error :user-not-owner-of-organisation
                                                         (format "Logged-in user with id %s does not own organisation with slug %s"
                                                                 logged-user-id
                                                                 organisation-slug))))
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :invalid-org-slug (helper/construct-model-error :slug-does-not-match-any-organisation
                                                                             (format "Organisation with slug %s was not found"
                                                                                     organisation-slug))
                             :user-not-owner-of-organisation message
                             :db-error (helper/construct-model-error :db-error
                                                                     (format "Database error occurred while validating organisation"))
                             (helper/construct-model-error :unknown-error
                                                           "Unknown error"))))))



