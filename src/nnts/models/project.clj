(ns nnts.models.project
  (:require [clojure.spec.alpha :as s]
            [nnts.models.helper :as helper]
            [nnts.db.project :as db-project]
            [nnts.db.helper :as db-helper]
            [nnts.db.organisation :as db-org]
            [failjure.core :as f]
            [nnts.db.core :as db-core]))


;; specS for valid project data

(s/def ::name (s/and string? not-empty))
(s/def ::description #(or (string? %) (nil? %) (empty %)))
(s/def ::slug (s/and string? not-empty))
(s/def ::organisation_id uuid?)


;; create spec for project structure
(s/def ::project (s/keys :req-un [::name ::description ::slug ::organisation_id]))

;; spec for date

(s/def ::date #(re-find #"\d{4}\-\d{2}\-\d{2}" %))


(defn create-project
  [project]
  (f/attempt-all []
                 (db-project/create-new-project (db-helper/get-conn) project)
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :duplicate-project (helper/construct-model-error
                                                 :project-already-exists
                                                 (format "Project %s already exists" project))
                             :org-for-new-proj-nonexistent (helper/construct-model-error
                                                            :project-creation-failed-organisation-does-not-exist
                                                            (format "Cannot create project %s. organisation %s does not exist"
                                                                    project
                                                                    (:organisation_id project)))
                             (helper/construct-unknown-error))))))

(defn validate-project-by-slug
  [project-slug organisation-id]
  (f/attempt-all [project (db-project/get-project-by-slug-and-organisation-id (db-helper/get-conn)
                                                                              project-slug
                                                                              organisation-id)]
                 (:id project)
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :invalid-slug-and-org-id-combo (helper/construct-model-error
                                                             :invalid-project-slug
                                                             (format "Project with slug %s not found in organisation with id %s"
                                                                     project-slug
                                                                     organisation-id))
                             (helper/construct-unknown-error))))))

(defn validate-project-by-id
  "Returns organisation id if project is valid."
  [project-id])


(defn add-member-to-project
  [project-member]
  (f/attempt-all [member (db-project/add-member-to-project (db-helper/get-conn) project-member)]
                 member
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :duplicate-project-member (helper/construct-model-error
                                                        :project-member-already-exists
                                                        (format "Project member %s already exists" project-member))
                             (helper/construct-unknown-error))))))


(defn get-all-members-for-project
  [project-id]
  (f/attempt-all [members (db-project/get-all-users-by-project-id (db-helper/get-conn) project-id)]
                 members
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             (helper/construct-unknown-error))))))


(defn get-all-notes-for-project
  [project-id]
  (f/attempt-all [notes (db-project/get-all-notes-by-project-id (db-helper/get-conn) project-id)]
                 notes
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             (helper/construct-unknown-error))))))

(defn get-project-owner-or-member
  [project-id user-id]
  (f/attempt-all [owner (db-project/get-owner-of-project (db-helper/get-conn) project-id)]
                 (if (= owner user-id)
                   user-id
                   (f/fail (helper/construct-model-error
                            :project-owner-not-found
                            (format "Could not find owner of project %s"
                                    project-id))))
                 (f/when-failed [{:keys [message] :as e}]
                   (case (:error message)
                     :project-owner-not-found (f/attempt-all [entry (db-project/get-project-member
                                                                     (db-helper/get-conn) project-id user-id)]
                                                             entry
                                                             (f/fail (helper/construct-model-error
                                                                      :project-member-does-not-exist
                                                                      (format "User %s is not the owner or a member of project %s"
                                                                              user-id
                                                                              project-id))))
                     (helper/construct-unknown-error)))))

(defn create-note
  [note]
  (f/attempt-all [new-note (db-project/create-new-note (db-helper/get-conn) note)]
                 new-note
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :note-creation-failed (f/fail (helper/construct-model-error
                                                            :failed-to-create-note
                                                            (format "Failed to create note %s"
                                                                    note))))))))

(defn edit-note
  [note-id note]
  (f/attempt-all [updated-note (db-project/edit-note (db-helper/get-conn) note-id note)]
                 updated-note
                 (f/when-failed [{:keys [message] :as e}]
                   (case (:error message)
                     :edit-note-failed (f/fail (helper/construct-model-error
                                                :failed-to-edit-note
                                                (format "Failed to edit note %s"
                                                        note)))))))

(defn get-all-notes-for-project-by-note-date
  [note-date project-id]
  (f/attempt-all [notes (db-project/get-all-notes-by-note-date-and-project-id (db-helper/get-conn)
                                                                              note-date
                                                                              project-id)]
                 notes
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             (helper/construct-unknown-error))))))
