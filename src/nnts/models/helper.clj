(ns nnts.models.helper)


(defn construct-model-error
  ([error]
   (construct-model-error error ""))
  ([error human-readable-message]
   {:type :model-error
    :error error
    :human-readable-message human-readable-message}))


(defn construct-unknown-error
  []
  {:type :model-error
   :error :unknown-error
   :human-readable-message "An unknown error occurred"})

