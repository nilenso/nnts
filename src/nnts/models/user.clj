(ns nnts.models.user
  (:require [nnts.config :as config]
            [nnts.models.helper :as helper]
            [nnts.db.helper :as db-helper]
            [nnts.db.user :as db-user]
            [failjure.core :as f]))


(defn create-user
  "Insert the user into the users table."
  [user]
  (db-user/create-or-update-user (db-helper/get-conn) user))


(defn validate-user-id
  "Check that the given user exists."
  [user-id]
  (f/attempt-all [valid-user-id (db-user/check-user-id-exists (db-helper/get-conn) user-id)]
                 valid-user-id
                 (f/when-failed [{:keys [message] :as e}]
                   (f/fail (case (:error message)
                             :user-does-not-exist (helper/construct-model-error
                                                   :validation-failed-user-not-found
                                                   (format "User with id %s was not found" user-id))
                             (db-helper/construct-db-error :database-error))))))

