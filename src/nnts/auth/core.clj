(ns nnts.auth.core
  (:require [clojure.string :as string]
            [clj-http.client :as client]
            [nnts.config :as config]
            [clojure.data.json :as json]))


(defn- construct-endpoint-url
  [id-token]
  (str (config/google-auth-tokeninfo-endpoint) "id_token=" id-token))


(defn- check-aud-contains-browser-client-id
  [aud]
  (string/includes? aud (config/google-auth-client-id :browser)))


(defn- extract-user-profile-data
  [auth-resp-body]
  {:google_user_id (get auth-resp-body "sub"),
   :first_name (get auth-resp-body "given_name")
   :last_name (get auth-resp-body "family_name")
   :photo_url (get auth-resp-body "picture")
   :email (get auth-resp-body "email")})


(defn get-user-for-id-token
  "verify that the id token is valid - returns the user profile if success else nil"
  [id-token]
  (let [auth-resp (client/get (construct-endpoint-url id-token))
        auth-resp-body (json/read-str (get auth-resp :body))]
    (if (check-aud-contains-browser-client-id (get auth-resp-body "aud"))
      (extract-user-profile-data auth-resp-body))))
