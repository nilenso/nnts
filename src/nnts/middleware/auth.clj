(ns nnts.middleware.auth
  (:require [nnts.auth.core :as auth]
            [nnts.db.user :as db-user]
            [nnts.db.helper :as helper]
            [nnts.handlers.helper :as resp-helper]
            [nnts.db.organisation :as db-org]))


(defn authenticate-user
  "Verify the given id-token live against Google's endpoint."
  [id-token]
  (when-let [user (auth/get-user-for-id-token id-token)]
    (when-let [google-user-id (get user :google_user_id)]
      (when-let [id (db-user/get-user-id-for-google-user-id (helper/get-conn) google-user-id)]
        id))))


(defn wrap-auth
  "Middleware for handling authentication."
  [handler]
  (fn [req]
    (if-let [session_id (get-in (:cookies req) ["session_id" :value])]
      (if-let [id (authenticate-user session_id)]
        (handler (assoc req :authenticated-user-id id))
        (resp-helper/create-response "USER-UNAUTHENTICATED" 401))
      (resp-helper/create-response  "USER-UNAUTHENTICATED" 401))))
