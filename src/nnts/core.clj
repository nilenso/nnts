(ns nnts.core
  (:gen-class)
  (:require [org.httpkit.server :refer [run-server]]
            [bidi.ring :refer [make-handler]]
            [nnts.handlers.login :as login]
            [nnts.handlers.organisation :as org]
            [nnts.handlers.project :as proj]
            [nnts.middleware.auth :refer [wrap-auth]]
            [nnts.config :as config]
            [selmer.parser :as selmer]
            [clojure.tools.nrepl.server :as nrepl]
            [cider.nrepl :refer (cider-nrepl-handler)]
            [migratus.core :as migratus]
            [ring.util.response :as res]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]))

(defonce server (atom nil))

(defn status-handler [req]
  (res/response "Status: OK"))


(def handler
  (-> (make-handler ["/" {"status" status-handler
                          "login" {:get login/get-login
                                   :post login/post-login}
                          "notes" {:get (wrap-auth proj/get-all-notes)}
                          ["organisations/" :organisation-slug "/projects/" :project-slug "/members"]
                          {:get (wrap-auth proj/get-list-members)
                           :post (wrap-auth proj/post-create-member)}
                          ["organisations/" :organisation-slug "/projects/" :project-slug "/notes"]
                          {:get (wrap-auth proj/get-list-notes)
                           :post (wrap-auth proj/post-create-note)}
                          ["organisations/" :organisation-slug "/projects/" :project-slug "/notes/" :note-id]
                          {:put (wrap-auth proj/put-edit-note)}

                          ["organisations/" :organisation-slug "/projects"] {:post (wrap-auth proj/post-create-project)}

                          "organisations/create" {:get (wrap-auth org/get-create-organisation)
                                                  :post (wrap-auth org/post-create-organisation)}

                          "favicon.ico" status-handler}])
      wrap-cookies
      wrap-params
      wrap-json-response
      (wrap-json-body {:keywords? false})))

(defn start-nrepl-server
 []
  (when-let [nrepl-port (config/nrepl-port)]
    (println "Starting NREPL server on port " nrepl-port)
    (nrepl/start-server :port nrepl-port :handler cider-nrepl-handler)))


(defn- start-server []
  (selmer/set-resource-path! (clojure.java.io/resource "html-templates"))
  (let [port (Integer/parseInt (config/http-listen-port))]
    (println "Running on port " port)
    (reset! server (run-server #'handler {:port port})))
  (start-nrepl-server))

(defn stop-server []
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))


(defn -main
  [& args]
  (config/load-config)

  (case (first args)
    "migrate" (migratus/migrate (config/migration-config))
    "init-db" (migratus/init (config/migration-config))
    (start-server)))
