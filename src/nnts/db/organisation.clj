;;;
;;; This module handles CRUD operations for the `organisations` table.
;;;

(ns nnts.db.organisation
  (:require [nnts.db.core :as core]
            [nnts.db.helper :as db-helper]
            [failjure.core :as f]))


(defn create-new-organisation
  "Create a new entry for an organisation with the given data."
  [conn org]
  (try
    (->> org
         (core/insert-row conn :organisations)
         first)
    (catch Exception e
      (f/fail (db-helper/construct-db-error e)))))


(defn get-organisation-by-slug
  "Retrieve entry for org by slug, if available else nil."
  [conn slug]
  (try
    (let [organisation (first (core/retrieve-all-rows-where conn "organisations"  "slug" slug))]
      (if organisation
        organisation
        (f/fail (db-helper/construct-db-error :invalid-org-slug))))
    (catch Exception e
      (f/fail (db-helper/construct-db-error :db-error)))))


(defn get-organisation-id-by-slug
  "Retrieve entry for org id by slug, if available, elese nil."
  [conn slug]
  (try
    (let [organisation (first (core/retrieve-all-rows-where conn "organisations"  "slug" slug))]
      (when-not organisation
        (f/fail (db-helper/construct-db-error :invalid-org-slug)))
      (:id  organisation))
    (catch Exception e
      (f/fail (db-helper/construct-db-error :db-error)))))


