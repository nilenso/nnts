(ns nnts.db.helper
  (:require [nnts.config :as config]))


(defn construct-db-error
  [error]
  {:type :db-error :error error})


(defn get-conn
  "Returns a new DB connection."
  []
  (config/db-uri))
