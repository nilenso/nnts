;;;
;;; This module handles the CRUD operations for the `projects` table.
;;;

(ns nnts.db.project
  (:require [nnts.db.core :as db]
            [nnts.db.helper :as db-helper]
            [nnts.helpers.core :as helper]
            [failjure.core :as f]
            [clj-time.core :as time]
            [clj-time.coerce :as coerce]
            [honeysql.core :as sql]
            [honeysql.helpers :as h]))


(defn create-new-project
  "Create a new project with the given data."
  [conn proj-row]
  (try
    (->> proj-row
         (db/insert-row conn :projects)
         first)
    (catch Exception e
      (cond
        (re-find #"duplicate key value violates unique constraint"
                 (.getLocalizedMessage e)) (f/fail (db-helper/construct-db-error
                                                    :duplicate-project))
        (re-find #"insert or update on table \"projects\" violates foreign key constraint"
                 (.getLocalizedMessage e)) (f/fail (db-helper/construct-db-error
                                                    :organisation-for-new-project-does-not-exist))
        :default (f/fail (db-helper/construct-db-error e))))))


(defn get-project-by-name-and-organisation-id
  "Retrieve a project by its name and organisation id."
  [conn name org-id]
  (let [project (first (db/retrieve-all-rows-where conn "projects" "name" name "organisation_id" org-id))]
    (when-not project
      (f/fail (db-helper/construct-db-error :invalid-name-and-org-id-combo)))
    project))


(defn get-project-by-slug-and-organisation-id
  "Retrieve a project by its slug and organisation id."
  [conn slug org-id]
  (let [project (first (db/retrieve-all-rows-where conn "projects" "slug" slug "organisation_id" org-id))]
    (if project
      project
      (f/fail (db-helper/construct-db-error :invalid-slug-and-org-id-combo)))))


(defn add-member-to-project
  "Add the given member to the given project.
  Returns the added row."
  [conn member]
  (try
    (-> (db/insert-row conn :project_members member)
        first)
    (catch Exception e
      (cond
        (re-find #"duplicate key value violates unique constraint"
                 (.getLocalizedMessage e)) (f/fail (db-helper/construct-db-error
                                                    :duplicate-project-member))
        :default (f/fail (db-helper/construct-db-error e))))))


(defn get-all-users-by-project-id
  "Return all users associated with the given project id."
  [conn project-id]
  (db/retrieve-all-rows-where conn "project_members" "project_id" project-id))


(defn get-all-notes-by-project-id
  "Retrieve all notes for the given project."
  [conn project-id]
  (db/retrieve-all-rows-where conn "project_notes" "project_id" project-id))


(defn get-project-member
  "Retrieve the project member by project id and user id."
  [conn project-id user-id]
  (let [member (db/retrieve-all-rows-where conn "project_members" "project_id" project-id "user_id" user-id)]
    (if (empty? member)
      (f/fail (db-helper/construct-db-error :project-member-not-found))
      (first member))))


(defn- find-by-id
  [conn table id-value]
  (-> (db/retrieve-all-rows-where conn table "id" id-value)
      first))


(defn get-owner-of-project
  "Retrieve the user id for the owner of the current project."
  [conn project-id]
  (let [organisation-id (-> (find-by-id conn "projects" project-id)
                            :organisation_id)
        owner-id (-> (find-by-id conn "organisations" organisation-id)
                     :owner_id)]
    (if-not owner-id
      (f/fail (db-helper/construct-db-error :project-owner-not-found))
      owner-id)))


(defn create-new-note
  "Create a brand new valid note."
  [conn project-note]
  (try
    (let [note-date (coerce/to-sql-date (:note_date project-note))
          time-stamp (coerce/to-sql-time (time/now))
          note (db/insert-row conn "project_notes" (assoc project-note
                                                          :note_date note-date
                                                         :note_time_utc time-stamp))]
     (if-not note
       (f/fail (db-helper/construct-db-error :note-creation-failed))
       note))
    (catch Exception e
      (f/fail (db-helper/construct-db-error :note-creation-failed)))))


(defn edit-note
  [conn id project-note]
  (try
    (let [note (assoc project-note :note_date (coerce/to-sql-date (:note_date project-note)))]
      (db/update-row-by-id conn "project_notes" id note))
    (catch Exception e
      (f/fail (db-helper/construct-db-error :edit-note-failed)))))


(def all-notes-query-map
  (-> (h/select :project_notes.* :users.first_name :users.last_name)
      (h/from :project_notes)
      (h/left-join :users [:= :project_notes.user_id :users.id])
      (h/where [:and
                [:= :project_notes.project_id (sql/param :project-id)]
                [:= :project_notes.note_date (sql/param :note-date)]])))

(defn get-all-notes-by-note-date-and-project-id
  [conn note-date project-id]
  (try
    (let [note-sql-date (coerce/to-sql-date note-date)
          project-id-uuid (helper/string->uuid project-id)
          notes-query (sql/format all-notes-query-map {:project-id project-id-uuid :note-date note-sql-date})]
      (db/run-custom-query conn notes-query))
    (catch Exception e
      (f/fail (db-helper/construct-db-error :get-all-notes-for-project-by-date-failed)))))
