;;;
;;; This module defines the raw operations of
;;; interacting with the database.
;;; Higher level functionality is provided in relevant namespaces.
;;;

(ns nnts.db.core
  (:require [clojure.java.jdbc :as jdbc]))


(defn retrieve-all-rows
  "retrieve all the rows in the table"
  [conn table]
  (jdbc/query conn (str "select * from " table)))


(defn retrieve-all-rows-where
  "retrieve all the rows in the table for the given where clause(s)"
  ([conn table col-key col-value]
   (jdbc/query conn [(str "select * from " table " where " col-key " = ?") col-value]))
  ([conn table col-key-1 col-value-1 col-key-2 col-value-2]
   (jdbc/query conn [(str "select * from " table " where " col-key-1 " = ? and " col-key-2 " = ?") col-value-1 col-value-2])))


(defn is-row-present
  "check if the row is present using col-key"
  [conn col-key col-value table]
  (-> (retrieve-all-rows-where conn table col-key col-value)
      not-empty))


(defn insert-row
  "insert a row into the table, returns the inserted row"
  [conn table row]
  (-> (jdbc/insert! conn table row)
      first))


(defn update-row
  "update the already existing row"
  [conn col-key col-value table row]
  (-> (jdbc/update! conn table row [(str col-key " = ?") col-value])))


(defn update-row-by-id
  [conn table id params]
  (jdbc/update! conn table params ["id = ?::uuid" id]))


(defn run-custom-query
  "Run the honeysql query"
  [conn query]
  (jdbc/query conn query))
