;;;
;;; This modules handles CRUD operations for the `user` table.
;;;
(ns nnts.db.user
  (:require [nnts.db.core :as db-core]
            [nnts.db.helper :as db-helper]
            [failjure.core :as f]))


(defn create-or-update-user
  "If the user already exists, update the user else insert the user."
  [conn user]
  (let [id (get user :google_user_id)]
    (if (db-core/is-row-present conn "google_user_id" id "users")
      (db-core/update-row conn "google_user_id" id "users" user)
      (db-core/insert-row conn "users" user))))


(defn get-user-id-for-google-user-id
  "Return the id for the user with the given google user id else nil."
  [conn google-user-id]
  (let [users (db-core/retrieve-all-rows-where conn "users" "google_user_id" google-user-id)]
    (if-let [user (first users)]
      (:id user))))


(defn check-user-id-exists
  "Check that the given user id refers to a valid user in the current database."
  [conn user-id]
  (try
    (let [user (first (db-core/run-custom-query conn (str "select * from users where id = '" user-id "'")))]
      (if user
        (:id user)
        (f/fail (db-helper/construct-db-error :user-does-not-exist))))
    (catch Exception e
      (println "exception e ZDES' = " e)
      (f/fail (db-helper/construct-db-error e)))))
