(defproject nnts "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [aero "1.1.3"]
	         [clj-http "3.9.1"]
                 [selmer "1.12.2"]
                 [ring "1.7.0"]
                 [ring/ring-mock "0.3.2"]
                 [ring/ring-json "0.4.0"]
                 [bidi "2.1.4"]
                 [migratus "1.1.2"]
                 [org.postgresql/postgresql "42.2.5.jre7"]
                 [org.clojure/java.jdbc "0.7.8"]
                 [org.clojure/data.json "0.2.6"]
                 [failjure "1.3.0"]
                 [http-kit "2.2.0"]
                 [clj-time  "0.15.0"]
                 [cider/cider-nrepl "0.18.0"]
                 [honeysql "0.9.4"]]
  :profiles {:dev { :dependencies [[venantius/pyro "0.1.2"]]
                   :injections [(require '[pyro.printer :as printer])
                                (printer/swap-stacktrace-engine!)] }
             :uberjar {:aot :all}}

  :main ^:skip-aot nnts.core
  :target-path "target/%s")
